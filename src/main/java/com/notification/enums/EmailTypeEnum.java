package com.notification.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;

@Getter
public enum EmailTypeEnum {

    WELCOME("welcome-email");

    private final String type;

    private static final Map<String, EmailTypeEnum> map = new HashMap<String, EmailTypeEnum>();

    static {
        for (EmailTypeEnum tipoEmailEnum : EmailTypeEnum.values())
            map.put(tipoEmailEnum.type, tipoEmailEnum);
    }

    public static EmailTypeEnum from(String value) {
        return map.get(value);
    }

    public static Collection<EmailTypeEnum> getAll() {
        return map.values();
    }

    EmailTypeEnum(String type){
        this.type = type;
    }

}