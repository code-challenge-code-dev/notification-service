package com.notification.kafka.consumer;

import com.notification.domain.services.EmailService;
import com.notification.enums.EmailTypeEnum;
import com.notification.kafka.dtos.SendNotificationMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationToSendTopicConsumerListenerService {

    @Value("${topic.name.notification-to-send}")
    private String topicName;

    private final EmailService emailService;

    @KafkaListener(
        topics = "${topic.name.notification-to-send}",
        groupId = "group_id"
    )
    public void consume(ConsumerRecord<String, SendNotificationMessage> payload) throws Exception {

        emailService.sendEmail(
            EmailTypeEnum.WELCOME.getType(),
            "Boas vindas ao Code Challenge",
            "Matheus",
            payload.value().emailTo());

        log.info("NotificationApplication");
        log.info("Topic: {}", topicName);
        log.info("key: {}", payload.key());
        log.info("Headers: {}", payload.headers());
        log.info("Partition: {}", payload.partition());
        log.info("Response: {}", payload.value());
    }

}