package com.notification.kafka.producer;

import com.notification.domain.usecases.kafka.NotificationSentTopicProducerUseCase;
import com.notification.kafka.dtos.NotificationSentMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationSentTopicProducerService implements NotificationSentTopicProducerUseCase {

    @Value("${topic.name.notification-sent}")
    private String topicName;

    private final KafkaTemplate<String, NotificationSentMessage> kafkaTemplate;

    @Override
    public void execute(NotificationSentMessage message){
        log.info("Payload sent to topic:{} {}", topicName, message);
        kafkaTemplate.send(topicName, message);
    }

}