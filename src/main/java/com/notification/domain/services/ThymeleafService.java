package com.notification.domain.services;

import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
@RequiredArgsConstructor
public class ThymeleafService {

    private final TemplateEngine templateEngine;

    public String templateEngineEmailThymeleaf(String template, Map<String, String> params) {
        Context context = new Context();
        params.entrySet().stream()
            .forEach((e) -> context.setVariable(e.getKey(), e.getValue()));
        return templateEngine.process(template, context);
    }

}