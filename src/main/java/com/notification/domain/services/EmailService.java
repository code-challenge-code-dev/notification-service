package com.notification.domain.services;

import jakarta.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class EmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    @Value("${spring.from.email}")
    private String emailFrom;

    private final JavaMailSender javaMailSender;

    private final ThymeleafService thymeleafService;

    @Transactional(rollbackFor = Exception.class)
    public void sendEmail(String template, String subject, String name, String email)
        throws Exception {
        try {
            String process = getTemplateEmail(template, name, email);
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);

            helper.setSubject(name + ", " + subject);
            helper.setFrom(emailFrom);
            helper.setTo(email);
            helper.setText(process, true);

            javaMailSender.send(mimeMessage);
            logger.info("Email sent");
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Something wrong trying to send the email: \n" + e.getMessage());
        }
    }


    public String getTemplateEmail(String template, String name, String email) {
        Map<String, String> _map = new HashMap<>();
        _map.put("name", name);

        return thymeleafService.templateEngineEmailThymeleaf(template, _map);
    }
}

