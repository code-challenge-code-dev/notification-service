package com.notification.domain.usecases.kafka;

import com.notification.kafka.dtos.NotificationSentMessage;

public interface NotificationSentTopicProducerUseCase {
    void execute(NotificationSentMessage communicationMessage);
}
