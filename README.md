# Processo seletivo desenvolvedor fullstack Java/Angular

##### Requisitos técnicos obrigatórios
- [ ] Java
- [ ] Spring Boot
- [ ] API/Webservices REST
- [ ] JUnit
- [ ] Angular 17 / TypeScript
- [ ] Banco de dados Postgres;
- [ ] Metodologias ágeis;
- [ ] GitLab / CI/CD;
- [ ] Docker
- [ ] AWS

## > > Desafio: Sistema de Gerenciamento de Usuários < <

### Contexto - Geral
- [ ] Este sistema deve permitir que os administradores realizem operações CRUD (Criar, Pesquisar, Atualizar e Deletar) usuários.

### Requisitos Técnicos - Específico

1. [ ] **Java 17 ou superior**: O backend do sistema deve ser desenvolvido em Java, utilizando as funcionalidades mais recentes disponíveis.
2. [ ] **Angular 17**: A interface de usuário deve ser construída utilizando Angular 17, com operações CRUD para gerenciar usuários.
3. [ ] **JPA e Hibernate**: Utilize JPA (Java Persistence API) e Hibernate para mapeamento objeto-relacional e realizar operações de banco de dados.
4. [ ] **Flyway**: Utilize o Flyway para gerenciar a migração do esquema do banco de dados. Certifique-se de que o esquema do banco de dados seja versionado e mantenha-se consistente com as alterações no código.
5. [ ] **Spring Boot**: Utilize o Spring Boot para configurar e desenvolver o backend do sistema. Aproveite ao máximo as convenções e funcionalidades oferecidas pelo Spring Boot.
6. [ ] **Controle de Exceções**: Implemente um controle de exceções para validar e tratar os dados fornecidos pelo usuário. Certifique-se de fornecer mensagens de erro significativas e tratamentos adequados para situações inesperadas.
7. [ ] **Casos de Uso de Usuários**: Implemente os seguintes casos de uso relacionados a usuários:
    - [ ] Cadastro de novo usuário.
    - [ ] Listagem de usuários cadastrados.
    - [ ] Visualização detalhada de um usuário específico.
    - [ ] Atualização dos dados de um usuário.
    - [ ] Exclusão de um usuário.
8. [ ] **Entidade Departamento**:
   - [ ] Implemente uma entidade "Departamento" para separar os usuários no sistema por departamento. Cada usuário deve ser associado a um único departamento.

### Requisitos Adicionais e Uso de Outras Tecnologias
- [ ] Internationalization with MessageSource
- [ ] Error Handler com ControllerAdvice
- [ ] Notification Service utilizando Kafka broker
- [ ] Utilização de Clean Architecture ou Ports And Adapters
- [ ] Unit Tests with H2 database
- [ ] Deploy na AWS com EKS
- [ ] Aplicação de Patterns

### Entrega

- [ ] O código-fonte do projeto deve ser entregue em um repositório Git (por exemplo, GitHub, GitLab, Bitbucket) e enviado para o email codedevelop.contato@gmail.com, envie também nesse email o seu curículo atualizado.
- [ ] É uma boa prática fazer commits de forma incremental durante o desenvolvimento, fornecendo uma narrativa clara da evolução do projeto.

### Critérios de Avaliação

1. **Funcionalidade Completa**: 
   - [ ] O sistema deve funcionar conforme os requisitos especificados, permitindo que os usuários realizem todas as operações CRUD de forma eficiente.
2. **Qualidade do Código**: 
   - [ ] O código deve ser limpo, modular e seguir as melhores práticas de desenvolvimento em Java e Angular.
     - Deve ser facilmente compreensível e passível de manutenção.
3. **Testabilidade**: 
   - [ ] O código deve ser testável
   - [ ] Tstes unitários
   - [ ] Testes de integração cobrindo as principais funcionalidades do sistema
4. **Documentação**: 
   - [ ] Forneça uma documentação clara e concisa
   - [ ] Descrevendo a arquitetura do sistema 
   - [ ] As tecnologias utilizadas
   - [ ] Instruções para configurar e executar o projeto localmente